package io.github.ytam.yemekler.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.ytam.yemekler.R;
import io.github.ytam.yemekler.adapter.FoodAdapter;
import io.github.ytam.yemekler.model.FootItem;
import io.github.ytam.yemekler.network.GetDataService;
import io.github.ytam.yemekler.network.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FoodActivity extends AppCompatActivity {

    private static final String TAG = "FoodActivity";
    private FoodAdapter adapter;
    ProgressDialog progressDoalog;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @BindView(R.id.lst_view) ListView lstView;
    @BindView(R.id.bottom_navigation) BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);
        ButterKnife.bind(this);


        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        progressDoalog = new ProgressDialog(FoodActivity.this);
        progressDoalog.setMessage("Yükleniyor....");
        progressDoalog.show();

        /*Create handle for the RetrofitInstance interface*/
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);

        Call<List<FootItem>> call = service.getAllAnnouncement();
        call.enqueue(new Callback<List<FootItem>>() {

            @Override
            public void onResponse(Call<List<FootItem>> call, Response<List<FootItem>> response) {
                progressDoalog.dismiss();
//                Log.d(TAG, "onResponse: "+response.body());
                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<FootItem>> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(FoodActivity.this, t+"Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });


    }


    private void generateDataList(List<FootItem> foodList) {

        Log.d(TAG, "onResponse: "+foodList.get(1).getRestaurantName());

        adapter = new FoodAdapter(getApplicationContext(), foodList);
        lstView.setAdapter(adapter);

    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_food_list:


                    return true;
                case R.id.action_log_out:

                    showPopUpMessage();

                    return true;

            }
            return false;
        }
    };



    public void showPopUpMessage( ) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FoodActivity.this);

        alertDialogBuilder.setTitle("Uyarı");

        alertDialogBuilder
                .setMessage("Çıkmak istediğinizden emin misiniz?")
                .setCancelable(false)
                .setIcon(R.drawable.ic_sentiment_dissatisfied_black_24dp)

                .setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Çıkış Yap", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        prefs = getSharedPreferences("loginInfo", MODE_PRIVATE);
                        editor = prefs.edit();
                        editor.putBoolean("is_login", false);
                        editor.apply();

                        finish();


                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }


}
