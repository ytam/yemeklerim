package io.github.ytam.yemekler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.github.ytam.yemekler.R;
import io.github.ytam.yemekler.model.FootItem;

public class FoodAdapter extends BaseAdapter {

    private List<FootItem> dataList;
    private Context context;

    public FoodAdapter(Context context, List<FootItem> dataList){
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_food, parent,false);
        }


        final View finalConvertView = convertView;
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "Clicked me", Toast.LENGTH_SHORT).show();

                LinearLayout second = finalConvertView.findViewById(R.id.second);

                if (second.getVisibility() == View.GONE){

                    second.setVisibility(View.VISIBLE);
                }else {

                    second.setVisibility(View.GONE);
                }

            }
        });


        TextView hastaAdi = convertView.findViewById(R.id.txtName);
        TextView hastaAdi2 = convertView.findViewById(R.id.txtName2);
        TextView hastaAdi3 = convertView.findViewById(R.id.txtName3);
        TextView hastaAdi4 = convertView.findViewById(R.id.txtName4);
        hastaAdi.setText( dataList.get(position).getRestaurantName());
        hastaAdi2.setText( dataList.get(position).getRestaurantName());
        hastaAdi3.setText( dataList.get(position).getRestaurantName());
        hastaAdi4.setText( dataList.get(position).getRestaurantName());


        return convertView;
    }



}
