package io.github.ytam.yemekler.network;

import java.util.List;

import io.github.ytam.yemekler.model.FootItem;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GetDataService {

    @GET("/mobiversite/restaurant.json")
    Call<List<FootItem>> getAllAnnouncement();


}
