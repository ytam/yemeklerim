package io.github.ytam.yemekler.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.ytam.yemekler.R;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edt_username) EditText edtUsername;
    @BindView(R.id.edt_password) EditText edtPassword;
    @BindView(R.id.btn_login) Button btnLogin;
    @BindView(R.id.switch_remember_me) Switch swtRememberMe;


    private static final String TAG = "LoginActivity";

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        prefs = getSharedPreferences("loginInfo", MODE_PRIVATE);


        Boolean isLogin = prefs.getBoolean("is_login",false);


        if (isLogin.equals(true)){

            startActivity(new Intent(LoginActivity.this, FoodActivity.class));

        }

    }

    @OnClick(R.id.btn_login) void clickBtnLogin(View view){

        hideKeyboard(view);

        Log.d(TAG, "Clicked Login Button");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Giriş Yapılıyor...");
        progressDialog.show();

        String username = edtUsername.getText().toString().trim();
        String password = edtPassword.getText().toString().trim();

        loginUser(username,password);
        progressDialog.dismiss();

    }

    @Override
    public void onBackPressed() {

        finish();
    }

    private void loginUser(final String username, final String password) {

        if (username.equals("ankara") && password.equals("123hh4")){

            startActivity(new Intent(LoginActivity.this, FoodActivity.class));
            editor = prefs.edit();
            editor.putBoolean("is_login", true);

            if (swtRememberMe.isChecked()){

                editor.putString("username", username);
                editor.putString("password", password);



            }else {

                editor.putString("username", "");
                editor.putString("password", "");
                edtUsername.getText().clear();
                edtPassword.getText().clear();
            }

            editor.apply();


        }else {
            Toast.makeText(this, "Kullanıcı adı ve ya şifre doğru değil.", Toast.LENGTH_SHORT).show();
        }

    }

    public void onLoginFailed() {

        Toast.makeText(getBaseContext(), "Giriş başarısız", Toast.LENGTH_LONG).show();
    }

    public boolean validate() {

        boolean valid = true;

        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();

        if (username.isEmpty() || username.length() < 4 || username.length() > 15) {

            edtUsername.setError("Geçerli bir kullanıcı adı giriniz.");
            valid = false;

        } else {

            edtUsername.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {

            edtPassword.setError("Şifreniz 4 ile 10 karakter arasında olmalıdır.");
            valid = false;

        } else {

            edtPassword.setError(null);
        }

        return valid;
    }

    public void hideKeyboard(View v) {

        InputMethodManager imm2 = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm2.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
