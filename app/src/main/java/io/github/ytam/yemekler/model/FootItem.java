package io.github.ytam.yemekler.model;

import com.google.gson.annotations.SerializedName;

public class FootItem {

    @SerializedName("date") private String date;
    @SerializedName("month") private String month;
    @SerializedName("restaurantName") private String restaurantName;
    @SerializedName("foodIngredients") private String foodIngredients;
    @SerializedName("state") private String state;
    @SerializedName("summary") private String summary;
    @SerializedName("summaryPrice") private Double summaryPrice;
    @SerializedName("foodPrice") private Double foodPrice;


    public FootItem(String date, String month, String restaurantName, String foodIngredients, String state, String summary, Double summaryPrice, Double foodPrice) {
        this.date = date;
        this.month = month;
        this.restaurantName = restaurantName;
        this.foodIngredients = foodIngredients;
        this.state = state;
        this.summary = summary;
        this.summaryPrice = summaryPrice;
        this.foodPrice = foodPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getFoodIngredients() {
        return foodIngredients;
    }

    public void setFoodIngredients(String foodIngredients) {
        this.foodIngredients = foodIngredients;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Double getSummaryPrice() {
        return summaryPrice;
    }

    public void setSummaryPrice(Double summaryPrice) {
        this.summaryPrice = summaryPrice;
    }

    public Double getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(Double foodPrice) {
        this.foodPrice = foodPrice;
    }
}
